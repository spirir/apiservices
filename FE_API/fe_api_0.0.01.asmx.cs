﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace FE_API
{
    /// <summary>
    /// Summary description for fe_api_0__0__01
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class fe_api_0__0__01 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld(string searchRQ)
        {
            return "Hello World from api, you have asked for :" + searchRQ;
        }
    }
}
